package com.pranavkapoorr.android_ws;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.pranavkapoorr.android_ws.databinding.ActivityMainBinding;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Map;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

public class MainActivity extends AppCompatActivity {
    private WebSocket ws;
    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        WebSocketFactory wsFactory = new WebSocketFactory();
        wsFactory.setSSLContext(getSSLContext());
        wsFactory.setVerifyHostname(false);

        try {
            ws = wsFactory.createSocket("wss://192.168.86.54/api");

            ws.addListener(new WebSocketAdapter() {
                @Override
                public void onConnected(WebSocket ws, Map<String, List<String>> headers) {
                    Log.d("MY_APP", "Connected");
                    ws.sendText("{\"transactionReference\":\"teswtMock\",\"paymentMethod\":\"Googlepay\",\"operation\":\"Payment\",\"tid\":\"multipay-tid2\",\"mid\":\"00007\",\"isMock\":true}");
                }

                @Override
                public void onTextMessage(WebSocket ws, String text) {
                    System.err.println("*************************************"+text+"***********************************************");
                    Log.d("MY_APP", "Message received: " + text);
                }
            });

            ws.connectAsynchronously();
        } catch (IOException e) {
            System.err.println("error:---------------------> "+e.getMessage());
            Log.e("APP","error:---------------------> "+e.getMessage());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    protected void onDestroy() {
        ws.disconnect();
        super.onDestroy();
    }

    private SSLContext getSSLContext(){
        SSLContext context;
        try {
            char[] password = "MultipayDev123".toCharArray();

            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(getApplicationContext().getResources().openRawResource(R.raw.multipaykeystore), password);

            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("PKIX");
            trustManagerFactory.init(keyStore);

            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("PKIX");
            keyManagerFactory.init(keyStore, password);

            context = SSLContext.getInstance("TLSv1.2");
            context.init(
                    keyManagerFactory.getKeyManagers(),
                    trustManagerFactory.getTrustManagers(),
                    new SecureRandom());
        } catch (KeyStoreException
                | IOException
                | NoSuchAlgorithmException
                | CertificateException
                | UnrecoverableKeyException
                | KeyManagementException e) {
            throw new RuntimeException(e);
        }
        return context;
    }

    private SSLSocketFactory getSSLFactory(){
        SSLSocketFactory fc = (SSLSocketFactory) SSLSocketFactory.getDefault();

        return  fc;
    }
}